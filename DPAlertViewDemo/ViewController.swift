//
//  ViewController.swift
//  DPAlertViewDemo
//
//  Created by Poter Hsu on 2015/8/13.
//  Copyright (c) 2015年 DamiPoter. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var alertManager: DPAlertManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        alertManager = DPAlertManager(fromViewController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onShowAlertButtonTouchUpInside(sender: AnyObject) {
        let alertViewController = self.storyboard!.instantiateViewControllerWithIdentifier("Alert")
        alertManager.showAlert(alertViewController)
    }

}


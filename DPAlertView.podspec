Pod::Spec.new do |s|
  s.name         = 'DPAlertView'
  s.version      = '1.0.4'
  s.platform     = :ios, '8.0'
  s.summary      = 'An alert view simulation for customizing layout.'
  s.description  = 'An alert view simulation for customizing layout with some animated transition.'
  s.homepage     = 'https://bitbucket.org/ChiaMingHsu/dpalertview'
  s.author	 = 'Poter Hsu'
  s.source       = {:git => 'https://ChiaMingHsu@bitbucket.org/ChiaMingHsu/dpalertview.git', :tag => '1.0.4'}
  s.source_files = 'DPAlertView/*.{swift}'
end


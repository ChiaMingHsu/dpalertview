//
//  DPAlertView.swift
//  DPAlertViewDemo
//
//  Created by Poter Hsu on 2015/8/13.
//  Copyright (c) 2015年 DamiPoter. All rights reserved.
//

import UIKit


public class DPAlertManager: NSObject, UIViewControllerTransitioningDelegate {
    
    var fromViewController: UIViewController
    let presentedAnimatedTransition = DPAlertPresentedAnimatedTransition()
    let dismissedAnimatedTransition = DPAlertDismissedAnimatedTransition()
    
    ///
    /// Notice that the instance of DPAlertManager should be alive until Alerting ViewController
    /// is dismissed.
    ///
    public init(fromViewController: UIViewController) {
        self.fromViewController = fromViewController
        
        super.init()
    }
    
    public func showAlert(alertViewController: UIViewController) {
        alertViewController.modalPresentationStyle = UIModalPresentationStyle.Custom
        alertViewController.transitioningDelegate = self
        
        fromViewController.presentViewController(alertViewController, animated: true, completion: nil)
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    
    public func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return presentedAnimatedTransition
    }
    
    public func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return dismissedAnimatedTransition
    }
    
    public func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
    public func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
}
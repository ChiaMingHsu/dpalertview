//
//  DPAlertDismissedAnimatedTransition.swift
//  DPAlertViewDemo
//
//  Created by Poter Hsu on 2015/8/13.
//  Copyright (c) 2015年 DamiPoter. All rights reserved.
//

import UIKit

class DPAlertDismissedAnimatedTransition : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.15
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let containerView = transitionContext.containerView()!
        let snapshotView = fromViewController.view.snapshotViewAfterScreenUpdates(false)
        
        containerView.sendSubviewToBack(toViewController.view)
        
        snapshotView.frame = fromViewController.view.frame
        containerView.addSubview(snapshotView)
        
        fromViewController.view.removeFromSuperview()
        toViewController.view.alpha = 1.0
        snapshotView.alpha = 1.0
        
        UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 2.5, options: .CurveLinear,
            animations: {
                snapshotView.alpha = 0
                toViewController.view.alpha = 1.0
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
    
}

//
//  DPAlertPresentingAnimatedTransition.swift
//  DPAlertViewDemo
//
//  Created by Poter Hsu on 2015/8/13.
//  Copyright (c) 2015年 DamiPoter. All rights reserved.
//

import UIKit

class DPAlertPresentedAnimatedTransition : NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.25
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let containerView = transitionContext.containerView()!
        
        fromViewController.view.alpha = 1.0
        toViewController.view.alpha = 0.0
        containerView.addSubview(toViewController.view)
        
        UIView.animateWithDuration(transitionDuration(transitionContext), delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 2.5, options: .CurveLinear,
            animations: {
                fromViewController.view.alpha = 0.3
                toViewController.view.alpha = 1.0
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
    
}
